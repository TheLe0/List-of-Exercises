#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(){
	float a,b,c,bask1,bask2,delta;
	
	printf("Coeficiente A: ");
	scanf("%f",&a);
	printf("Coeficiente B: ");
	scanf("%f",&b);
	printf("Coeficiente C: ");
	scanf("%f",&c);
	
	delta = pow(b,2) - 4*a*c;
	
	if (delta < 0)
		printf("Sem raizes reais.");
	else{
		bask1 = (-b + delta)/(2*a); 
		bask2 = (-b - delta)/(2*a); 
		printf("Raizes %0.2f e %0.2f",bask1,bask2);	
	}	
	
	system("pause");
}
