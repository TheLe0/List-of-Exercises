#include <stdlib.h>
#include <stdio.h>

int main(){
	int he,hs,dif;
	float tot;
	
	printf("HE: ");
	scanf("%d",&he);
	printf("HS: ");
	scanf("%d",&hs);
	
	dif = hs - he;
	
	if(dif<=2){
		tot = dif*5;
	}else if(dif>2 && dif<=4){
		dif = dif - 2;
		tot = 10 + dif*2;
	}else if(dif>4){
		dif = dif - 4;
		tot = 14 + dif;
	}
	
	printf("R$%0.2f",tot);
	
	
	system("pause");
}
