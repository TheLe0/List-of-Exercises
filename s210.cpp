#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(){
	
	int ax,ay,bx,by,axb;
	
	printf("Digite a coordenada X do ponto A: ");
	scanf("%d",&ax);
	printf("Digite a coordenada Y do ponto A: ");
	scanf("%d",&ay);
	printf("Digite a coordenada X do ponto B: ");
	scanf("%d",&bx);
	printf("Digite a coordenada Y do ponto B: ");
	scanf("%d",&by);
	
	axb = fabs(ax*by-ay*bx);
	
	printf("O produto vetorial equivale a %d unidades.",axb);
	
	system("pause");
}
