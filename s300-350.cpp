#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(){
	float a,b,c,bask,baskl,soma,produto;
	
	printf("Digite o valor de A: ");
	scanf("%f",&a);
	printf("Digite o valor de B: ");
	scanf("%f",&b);
	printf("Digite o valor de C: ");
	scanf("%f",&c);
	
	bask = -b + sqrt(pow(b,2) - 4*a*c);
	baskl = -b - sqrt(pow(b,2) - 4*a*c);
	
	soma = bask + baskl;
	produto = bask*baskl;
	
	printf("Soma: %0.2f\n",soma);
	printf("Produto: %0.2f\n",produto);
	
	system("pause");
}
