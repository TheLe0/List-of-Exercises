#include <stdio.h>
#include <stdlib.h>

int main(){
	int p[5],prog,i;
	
	printf("Progressao: ");
	scanf("%d",&prog);
	printf("Primeiro termo: ");
	scanf("%d",&p[0]);
	
	for(i=1;i<5;i++){
		p[i] = p[i-1] + prog;
	}
	
	for(i=0;i<5;i++){
		printf("%d ",p[i]);
	}
	
	system("pause");
}
